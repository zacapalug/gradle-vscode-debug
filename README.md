# GRADLE-VSCODE-DEBUG

```none
888888888888  88    ,ad8888ba,
     88       88   d8"'    `"8b                            ,d
     88       88  d8'        `8b                           88
     88       88  88          88             ,adPPYb,d8  MM88MMM
     88       88  88          88  aaaaaaaa  a8"    `Y88    88
     88       88  Y8,        ,8P  """"""""  8b       88    88
     88       88   Y8a.    .a8P             "8a,   ,d88    88,
     88       88    `"Y8888Y"'               `"YbbdP"Y8    "Y888
                                             aa,    ,88
                                              "Y8bbdP"
```

Este proyecto es para demostracion del proceso de debug dentro del IDE Visual Code
y la herramienta de build gradle.

Como requisito se debe de instalar el plugin: [Java Extension Pack](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-pack),
dentro de Visual Code.

## El proyecto

Se intento hacer un proyecto simple, en este caso utilizando el framework [spark](http://sparkjava.com/),
para crear servicios web y poder debugear los llamados a estos servicios.

Al ejecutar el proyecto se tienen disponibles los siguientes servicios:

* /demo
* /status
* /stop

## Tasks gradle

Para limpiar y compilar el proyecto utilizamos en task:

```bash
gradle clean build
```

Para ejecutar el projecto simplemente ejecutamos el task:

```bash
gradle run
```

Con lo cual ya tendremos nuestro proyecto levantado y escuchando en el puerto **8182**.
Y podemos realizar los request correspondientes.