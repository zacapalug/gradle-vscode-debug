package com.tio_gt;

import static spark.Spark.stop;

import spark.Request;
import spark.Response;

/**
 * Clase con la definicion de la logica a ejecutar por rutas.
 * 
 * Esta es una clase de utilidad, la cual pone a disposicion una serie de metodos estaticos.
 */
public final class Rutas {

    /**
     * Informacion a procesar y mostrar.
     */
    private static final String INFORMACION = "abeurp ed otxet nu se etsE";

    /**
     * Inicio de los tag por default de un documento html.
     */
    private static final String INICIO_TAG = "<html><body><p>";

    /**
     * Fin de los tag por default de un documento html.
     */
    private static final String FIN_TAG = "</p></body></html>";

    /**
     * Representacion de salto de linea en html.
     */
    private static final String SALTO_LINEA = "<br />";

    /**
     * Metodo para mostrar el resultado de un procesamiento.
     */
    public static String mostrarInformacion(Request req, Response res) {
        final StringBuilder informacion = new StringBuilder();
        informacion.append(INICIO_TAG);
        informacion.append(simulandoOperacion());
        informacion.append(FIN_TAG);

        return informacion.toString();
    }

    /**
     * Monstrando informacion del servidor.
     */
    public static String statusServer(Request req, Response res) {
        final StringBuilder status = new StringBuilder();
        status.append(INICIO_TAG)
            .append("URL: ").append(req.url()).append(SALTO_LINEA)
            .append("HEADER: ").append(req.headers()).append(SALTO_LINEA)
            .append("COOKIES: ").append(req.cookies()).append(SALTO_LINEA)
            .append("USER AGENT: ").append(req.userAgent())
            .append(FIN_TAG);

        return status.toString();
    }

    /**
     * Metodo para detener el servicio
     */
    public static String stopServer() {
        stop();

        return Mensajes.APAGANDO_SERVIDOR;
    }

    /**
     * Metodo dummy que simula el procesamiento de informacion.
     */
    private static String simulandoOperacion() {
        String informacionProcesada = new StringBuilder(INFORMACION).reverse().toString();
        informacionProcesada = informacionProcesada.toUpperCase();

        return informacionProcesada;
    }

    /**
     * Constructo necesario para clase utility.
     */
    Rutas() {
        //Uso por default.
    }
}