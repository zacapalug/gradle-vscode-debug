package com.tio_gt;

import static spark.Spark.*;

public class Main {

    private final static int PORT_APP = 8182;

    public static void main(String[] args) {
        port(PORT_APP);

        notFound(Mensajes.ELEMENTO_NO_ENCONTRADO);

        get("/stop", (req, res) -> Rutas.stopServer());

        get("/status", (req, res) -> Rutas.statusServer(req, res));

        get("/demo", (req, res) -> Rutas.mostrarInformacion(req, res));
    }
}
