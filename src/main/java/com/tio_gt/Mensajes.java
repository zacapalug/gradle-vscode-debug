package com.tio_gt;

/**
 * Clase con la definicion de mensajes.
 * 
 * Esta es una clase de utilidad, la cual pone a disposicion una serie de variables estaticas.
 */
public final class Mensajes {

    /**
     * Mensaje a mostar cuando un elemento no es mapeado.
     */
    public static final String ELEMENTO_NO_ENCONTRADO = "<html><body><h1>Elemento no encontrado!</h1></body></html>";

    /**
     * Mensaje a mostrar cuando se ejecuta el apagado del servidor.
     */
    public static final String APAGANDO_SERVIDOR = "<html><body><h1>Servidor apagado!</h1></body></html>";

    /**
     * Constructo necesario para clase utility.
     */
    Mensajes() {
        //Uso por default.
    }
}